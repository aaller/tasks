import flask
import flask_cors

from src.blueprints.tasks import tasks_crud
from src.blueprints.webapp import webapp


def create_app(debug=False, testing=False):

    """ Create the Flask application """

    app = flask.Flask(__name__, template_folder='templates')
    app.debug = False
    app.testing = False

    app.register_blueprint(webapp)
    app.register_blueprint(tasks_crud, url_prefix='/api/tasks')

    flask_cors.CORS(app)

    return app
